import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TDDTest {

    TDD t = new TDD();

    @Test
    void addieren() {
        assertEquals(0, t.addieren(0, 0));
        assertEquals(3, t.addieren(1, 2));
    }

}